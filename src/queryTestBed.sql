-- Brian Egana
-- begana@calpoly.edu

-- General Query 3 Part 1
SELECT stocks.Year, stocks.Name AS Top5StocksAbsoluteIncrease
FROM (
      SELECT YEAR(s.SDate) AS Year, s.Name,
          @rank :=
              IF(@current_date = YEAR(s.EDate), @rank + 1, 1) AS rank,
          @current_date := YEAR(s.EDate) AS CurDate
      FROM (
            SELECT YearsStart.SDate, YearsEnd.EDate, s.Name,
                ap2.Close - ap1.Open AS AbsDiff
            FROM Securities s, AdjustedPrices ap1, AdjustedPrices ap2,
                 (
                  SELECT MIN(ap.Day) AS SDate
                  FROM AdjustedPrices ap
                  GROUP BY YEAR(ap.Day)
                 ) YearsStart,
                 (
                  SELECT MAX(ap.Day) AS EDate
                  FROM AdjustedPrices ap
                  GROUP BY YEAR(ap.Day)
                 ) YearsEnd
            WHERE s.Ticker = ap1.Ticker AND s.Ticker = ap2.Ticker AND
                ap1.Day = YearsStart.SDate AND ap2.Day = YearsEnd.EDate AND
                YEAR(ap1.Day) = YEAR(ap2.Day)
            GROUP BY YearsStart.SDate, s.Name
            ORDER BY YearsStart.SDate, AbsDiff DESC
           ) s,
           (SELECT @rank := 0, @current_date := 0) variables
     ) stocks
WHERE stocks.rank <= 5;

-- General Query 3 Part 2
SELECT stocks.Year, stocks.Name AS Top5StocksRelativeIncrease
FROM (
      SELECT YEAR(s.SDate) AS Year, s.Name,
          @rank :=
              IF(@current_date = YEAR(s.EDate), @rank + 1, 1) AS rank,
          @current_date := YEAR(s.EDate) AS CurDate
      FROM (
            SELECT YearsStart.SDate, YearsEnd.EDate, s.Name,
                ((ap2.Close - ap1.Open) / ((ap2.Close + ap1.Open) / 2)) * 100
                AS RelDiff
            FROM Securities s, AdjustedPrices ap1, AdjustedPrices ap2,
                 (
                  SELECT MIN(ap.Day) AS SDate
                  FROM AdjustedPrices ap
                  GROUP BY YEAR(ap.Day)
                 ) YearsStart,
                 (
                  SELECT MAX(ap.Day) AS EDate
                  FROM AdjustedPrices ap
                  GROUP BY YEAR(ap.Day)
                 ) YearsEnd
            WHERE s.Ticker = ap1.Ticker AND s.Ticker = ap2.Ticker AND
                ap1.Day = YearsStart.SDate AND ap2.Day = YearsEnd.EDate AND
                YEAR(ap1.Day) = YEAR(ap2.Day)
            GROUP BY YearsStart.SDate, s.Name
            ORDER BY YearsStart.SDate, RelDiff DESC
           ) s,
           (SELECT @rank := 0, @current_date := 0) variables
     ) stocks
WHERE stocks.rank <= 5;

-- General Query 4
SELECT sectors.Sector, sectors.AvgDiff,
    IF(sectors.AvgDiff > 10, 'Significantly Outperforming',
       IF(sectors.AvgDiff > 5, 'Performing Well',
          IF(sectors.AvgDiff > 0, 'Showing Resilience', 'Tanking')
         )
      ) AS SectorPerformance
FROM (
      SELECT s.Sector,
          ROUND((AVG(ap2.Close) - AVG(ap1.Open)), 2) AS AvgDiff
      FROM Securities s, AdjustedPrices ap1, AdjustedPrices ap2
      WHERE s.Ticker = ap1.Ticker AND s.Ticker = ap2.Ticker AND
          ap1.Day =
          (
           SELECT MIN(ap.Day)
           FROM AdjustedPrices ap
           WHERE YEAR(ap.Day) = 2016
          ) AND ap2.Day =
          (
           SELECT MAX(ap.Day)
           FROM AdjustedPrices ap
           WHERE YEAR(ap.Day) = 2016
          )
      GROUP BY s.Sector
      ORDER BY AVG(ap2.Close) - AVG(ap1.Open) DESC
     ) sectors;

-- Individual Query 5
SELECT ap.Volume AS DayVol, Avg.Volume AS AvgVol, ap.Close AS DayClose,
    Avg.Close AS AvgClose,
    IF(ap.Volume < Avg.Volume, 'Hold',
       IF(ap.Close > Avg.Close, 'Buy', 'Sell')
      ) AS TradeDecision
FROM (
      SELECT AVG(Last3MonFromDate.Close) AS Close,
          AVG(Last3MonFromDate.Volume) AS Volume
      FROM (
            SELECT s.Ticker, ap.Day, ap.Close, ap.Volume
            FROM Securities s NATURAL JOIN AdjustedPrices ap
            WHERE s.Ticker = 'FISV' AND ap.Day < '2015-01-01' AND
                ap.Day >= '2014-10-01'
           ) Last3MonFromDate
     ) Avg, Securities s NATURAL JOIN AdjustedPrices ap
WHERE s.Ticker = 'FISV' AND (ap.Day = '2015-01-01' OR ap.Day =
    (
     SELECT MIN(Day)
     FROM Securities s NATURAL JOIN AdjustedPrices ap
     WHERE s.Ticker = 'FISV' AND MONTH(Day) = 01 AND YEAR(Day) = 2015
    ));

