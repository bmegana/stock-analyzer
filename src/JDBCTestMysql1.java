import java.sql.*;
import java.io.*;

public class JDBCTestMysql1 {

    public static void main(String args[]) {
        FileReader fileReader;
        BufferedReader credentialsReader;

        String user = null, password = null;
        String url = "jdbc:mysql://cslvm74.csc.calpoly.edu/";
        Connection conn = null;

        try {
            fileReader = new FileReader("credentials.in");
            credentialsReader = new BufferedReader(fileReader);

            user = credentialsReader.readLine();
            password = credentialsReader.readLine();
            fileReader.close();
        }
        catch (Exception ex) {
            System.out.println("Credentials not found");
        }

    	try {
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch (Exception ex) {
            System.out.println("Driver not found");
            System.out.println(ex);
        }


    	try {
            conn = DriverManager.getConnection(url + user + "?user=" + user +
                    "&password=" + password);
            System.out.println("Connected");
        }
        catch (Exception ex) {
    	    System.out.println("Could not open connection");
            System.out.println(ex);
        }

        assert conn != null;
        try {
            Statement s1 = conn.createStatement();
            String table = "CREATE TABLE Books ";
            table = table + "(LibCode INT, Title VARCHAR(50), Author VARCHAR (50),";
            table = table + "PRIMARY KEY (LibCode) )";

            System.out.println(table);
            // s1.executeUpdate("use dekhtyar");
            s1.executeUpdate(table);
        } catch (Exception ee) {
            System.out.println(ee);
        }

        try {
            Statement s2 = conn.createStatement();
            s2.executeUpdate("INSERT INTO Books VALUES(1, 'Database Systems','Ullman')");
            s2.executeUpdate(
                    "INSERT INTO Books VALUES(2, 'Artificial Intelligence', 'Russel, Norvig')");
            s2.executeUpdate(
                    "INSERT INTO Books VALUES(3, 'Problem Solving in C', 'Hanly, Koffman')");
        }  catch (Exception ee) {
            System.out.println(ee);
        }
   
        try {
            Statement s3 = conn.createStatement();
            ResultSet result = s3.executeQuery("SELECT Title, Author FROM Books");
            boolean f = result.next();
            while (f) {
                String s = result.getString(1);
                String a = result.getString(2);
                System.out.println(s+", "+ a);
                f=result.next();
            }
        }  catch (Exception ee) {
            System.out.println(ee);
        }
        
        try {
            String psText = "INSERT INTO Books VALUES(?,?,?)";
            PreparedStatement ps = conn.prepareStatement(psText);

            ps.setInt(1, 4);
            ps.setString(2, "A Guide to LaTeX");
            ps.setString(3, "Kopka, Daly");

            ps.executeUpdate();
        } catch (Exception e03) {
            System.out.println(e03);
        }

        try {
            Statement s4 = conn.createStatement();
            ResultSet result = s4.executeQuery("SELECT Title, Author FROM Books");
            boolean f = result.next();
            while (f) {
                String s = result.getString(1);
                String a = result.getString(2);
                System.out.println(s+", "+ a);
                f=result.next();
            }
            s4.executeUpdate("DROP TABLE Books");
        }  catch (Exception ee) {
            System.out.println(ee);
        }

        try {
            conn.close();
        }
        catch (Exception ex) {
            System.out.println("Unable to close connection");
        }
    }
}
