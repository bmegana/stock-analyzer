/*
 * GeneralStockAnalyzer.java
 *
 * Members: Brian Egana (begana@calpoly.edu)
 *          Tim Jyung (tjyung@calpoly.edu)
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.*;

public class GeneralStockAnalyzer {

    private static String user = null, password = null;
    private static Connection conn = null;
    private static ReportGenerator gen = new ReportGenerator();

    private static void ReadCredentials() {
        FileReader fileReader;
        BufferedReader credentialsReader;

        try {
            fileReader = new FileReader("credentials.in");
            credentialsReader = new BufferedReader(fileReader);

            user = credentialsReader.readLine();
            password = credentialsReader.readLine();
            fileReader.close();
        }
        catch (Exception ex) {
            System.out.println("Credentials not found");
        }
    }

    private static void InstantiateDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch (Exception ex) {
            System.out.println("Driver not found");
            System.out.println(ex);
        }
    }

    private static void ConnectToDatabase() {
        String url = "jdbc:mysql://cslvm74.csc.calpoly.edu/";

        try {
            conn = DriverManager.getConnection(url + "nyse" + "?user=" + user +
                    "&password=" + password);
            System.out.println("Connected");
        }
        catch (Exception ex) {
            System.out.println("Could not open connection");
        }
    }

    private static void GeneralQuery1() {
        String query =
            "SELECT t1.NumSecuritiesStart2016, t2.NumSecuritiesEnd2016,\n" +
            "    t3.NumPriceIncrease, t4.NumPriceDecrease\n" +
            "FROM (\n" +
            "      SELECT COUNT(s.Ticker) AS NumSecuritiesStart2016\n" +
            "      FROM Securities s NATURAL JOIN AdjustedPrices ap\n" +
            "      WHERE ap.Day = \n" +
            "          (\n" +
            "           SELECT MIN(ap.Day)\n" +
            "           FROM AdjustedPrices ap\n" +
            "           WHERE YEAR(ap.Day) = 2016\n" +
            "          )\n" +
            "     ) t1,\n" +
            "     (\n" +
            "      SELECT COUNT(s.Ticker) AS NumSecuritiesEnd2016\n" +
            "      FROM Securities s NATURAL JOIN AdjustedPrices ap\n" +
            "      WHERE ap.Day = \n" +
            "          (\n" +
            "           SELECT MAX(ap.Day)\n" +
            "           FROM AdjustedPrices ap\n" +
            "           WHERE YEAR(ap.Day) = 2016\n" +
            "          )\n" +
            "     ) t2,\n" +
            "     (\n" +
            "      SELECT COUNT(s.Ticker) AS NumPriceIncrease\n" +
            "      FROM Securities s, AdjustedPrices ap1, AdjustedPrices ap2\n" +
            "      WHERE s.Ticker = ap1.Ticker AND s.Ticker = ap2.Ticker\n" +
            "          AND ap1.Day = \n" +
            "          (\n" +
            "           SELECT MAX(ap.Day)\n" +
            "           FROM AdjustedPrices ap\n" +
            "           WHERE YEAR(ap.Day) = 2015\n" +
            "          ) AND ap2.Day = \n" +
            "          (\n" +
            "           SELECT MAX(ap.Day)\n" +
            "           FROM AdjustedPrices ap\n" +
            "           WHERE YEAR(ap.Day) = 2016\n" +
            "          ) AND ap1.Close < ap2.Close\n" +
            "     ) t3,\n" +
            "     (\n" +
            "      SELECT COUNT(s.Ticker) AS NumPriceDecrease\n" +
            "      FROM Securities s, AdjustedPrices ap1, AdjustedPrices ap2\n" +
            "      WHERE s.Ticker = ap1.Ticker AND s.Ticker = ap2.Ticker\n" +
            "          AND ap1.Day = \n" +
            "          (\n" +
            "           SELECT MAX(ap.Day)\n" +
            "           FROM AdjustedPrices ap\n" +
            "           WHERE YEAR(ap.Day) = 2015\n" +
            "          ) AND ap2.Day = \n" +
            "          (\n" +
            "           SELECT MAX(ap.Day)\n" +
            "           FROM AdjustedPrices ap\n" +
            "           WHERE YEAR(ap.Day) = 2016\n" +
            "          ) AND ap1.Close > ap2.Close\n" +
            "     ) t4;";

        try {
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery(query);
            boolean f = result.next();
            gen.CreateTable("Securities");

            gen.CreateRow();
            gen.InsertTableHeader("# Traded at Start of 2016");
            gen.InsertTableHeader("# Traded at End of 2016");
            gen.InsertTableHeader("# Increased in Price");
            gen.InsertTableHeader("# Decreased in Price");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);
                String s4 = result.getString(4);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.InsertTableData(s4);
                gen.FinishRow();

                f = result.next();
            }
            gen.FinishTable();
        }  catch (Exception ee) {
            System.out.println(ee);
        }
    }

    private static void GeneralQuery2() {
        String query =
            "SELECT s.Name\n" +
            "FROM Securities s NATURAL JOIN AdjustedPrices ap\n" +
            "WHERE YEAR(ap.Day) = 2016\n" +
            "GROUP BY s.Name\n" +
            "ORDER BY SUM(ap.Volume) DESC LIMIT 10;";

        try {
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery(query);
            boolean f = result.next();
            gen.CreateTable("Top 10 Traded Stocks");

            gen.CreateRow();
            gen.InsertTableHeader("Stock");
            gen.FinishRow();

            while (f) {
                String s = result.getString(1);
                gen.CreateRow();
                gen.InsertTableData(s);
                gen.FinishRow();
                f = result.next();
            }
            gen.FinishTable();
        }  catch (Exception ee) {
            System.out.println(ee);
        }
    }

    private static void GeneralQuery3() {
        String query1 =
            "SELECT stocks.Year, stocks.Name AS Top5StocksAbsoluteIncrease,\n" +
            "    stocks.rank AS Rank\n" +
            "FROM (\n" +
            "      SELECT YEAR(s.SDate) AS Year, s.Name,\n" +
            "          @rank :=\n" +
            "              IF(@current_date = YEAR(s.EDate), @rank + 1, 1) AS rank,\n" +
            "          @current_date := YEAR(s.EDate) AS CurDate\n" +
            "      FROM (\n" +
            "            SELECT YearsStart.SDate, YearsEnd.EDate, s.Name,\n" +
            "                ap2.Close - ap1.Open AS AbsDiff\n" +
            "            FROM Securities s, AdjustedPrices ap1, AdjustedPrices ap2,\n" +
            "                 (\n" +
            "                  SELECT MIN(ap.Day) AS SDate\n" +
            "                  FROM AdjustedPrices ap\n" +
            "                  GROUP BY YEAR(ap.Day)\n" +
            "                 ) YearsStart,\n" +
            "                 (\n" +
            "                  SELECT MAX(ap.Day) AS EDate\n" +
            "                  FROM AdjustedPrices ap\n" +
            "                  GROUP BY YEAR(ap.Day)\n" +
            "                 ) YearsEnd\n" +
            "            WHERE s.Ticker = ap1.Ticker AND s.Ticker = ap2.Ticker AND\n" +
            "                ap1.Day = YearsStart.SDate AND ap2.Day = YearsEnd.EDate AND\n" +
            "                YEAR(ap1.Day) = YEAR(ap2.Day)\n" +
            "            GROUP BY YearsStart.SDate, s.Name\n" +
            "            ORDER BY YearsStart.SDate, AbsDiff DESC\n" +
            "           ) s,\n" +
            "           (SELECT @rank := 0, @current_date := 0) variables\n" +
            "     ) stocks\n" +
            "WHERE stocks.rank <= 5;";

        String query2 =
            "SELECT stocks.Year, stocks.Name AS Top5StocksRelativeIncrease,\n" +
            "    stocks.rank AS Rank\n" +
            "FROM (\n" +
            "      SELECT YEAR(s.SDate) AS Year, s.Name,\n" +
            "          @rank :=\n" +
            "              IF(@current_date = YEAR(s.EDate), @rank + 1, 1) AS rank,\n" +
            "          @current_date := YEAR(s.EDate) AS CurDate\n" +
            "      FROM (\n" +
            "            SELECT YearsStart.SDate, YearsEnd.EDate, s.Name,\n" +
            "                ((ap2.Close - ap1.Open) / ((ap2.Close + ap1.Open) / 2)) * 100\n" +
            "                AS RelDiff\n" +
            "            FROM Securities s, AdjustedPrices ap1, AdjustedPrices ap2,\n" +
            "                 (\n" +
            "                  SELECT MIN(ap.Day) AS SDate\n" +
            "                  FROM AdjustedPrices ap\n" +
            "                  GROUP BY YEAR(ap.Day)\n" +
            "                 ) YearsStart,\n" +
            "                 (\n" +
            "                  SELECT MAX(ap.Day) AS EDate\n" +
            "                  FROM AdjustedPrices ap\n" +
            "                  GROUP BY YEAR(ap.Day)\n" +
            "                 ) YearsEnd\n" +
            "            WHERE s.Ticker = ap1.Ticker AND s.Ticker = ap2.Ticker AND\n" +
            "                ap1.Day = YearsStart.SDate AND ap2.Day = YearsEnd.EDate AND\n" +
            "                YEAR(ap1.Day) = YEAR(ap2.Day)\n" +
            "            GROUP BY YearsStart.SDate, s.Name\n" +
            "            ORDER BY YearsStart.SDate, RelDiff DESC\n" +
            "           ) s,\n" +
            "           (SELECT @rank := 0, @current_date := 0) variables\n" +
            "     ) stocks\n" +
            "WHERE stocks.rank <= 5;";

        try {
            Statement statement = conn.createStatement();
            Statement statement2 = conn.createStatement();
            ResultSet result = statement.executeQuery(query1);
            ResultSet result2 = statement2.executeQuery(query2);

            boolean f = result.next(), f2 = result2.next();
            gen.CreateTable("Top 5 Yearly Stocks in Terms of Price Increase");

            gen.CreateRow();
            gen.InsertTableHeader("Year");
            gen.InsertTableHeader("Stock (Absolute)");
            gen.InsertTableHeader("Rank");
            gen.InsertTableHeader("Stock (Relative)");
            gen.FinishRow();

            while (f || f2) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);
                String s4 = result2.getString(2);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.InsertTableData(s4);
                gen.FinishRow();

                f = result.next();
                f2 = result2.next();
            }
            gen.FinishTable();
        }  catch (Exception ee) {
            System.out.println(ee);
        }
    }

    private static void GeneralQuery4() {
        String query =
            "SELECT sectors.Sector, sectors.AvgDiff,\n" +
            "    IF(sectors.AvgDiff > 10, 'Significantly Outperforming',\n" +
            "       IF(sectors.AvgDiff > 5, 'Performing Well',\n" +
            "          IF(sectors.AvgDiff > 0, 'Showing Resilience', 'Tanking')\n" +
            "         )\n" +
            "      ) AS SectorPerformance\n" +
            "FROM (\n" +
            "      SELECT s.Sector,\n" +
            "          ROUND((AVG(ap2.Close) - AVG(ap1.Open)), 2) AS AvgDiff\n" +
            "      FROM Securities s, AdjustedPrices ap1, AdjustedPrices ap2\n" +
            "      WHERE s.Ticker = ap1.Ticker AND s.Ticker = ap2.Ticker AND\n" +
            "          ap1.Day =\n" +
            "          (\n" +
            "           SELECT MIN(ap.Day)\n" +
            "           FROM AdjustedPrices ap\n" +
            "           WHERE YEAR(ap.Day) = 2016\n" +
            "          ) AND ap2.Day =\n" +
            "          (\n" +
            "           SELECT MAX(ap.Day)\n" +
            "           FROM AdjustedPrices ap\n" +
            "           WHERE YEAR(ap.Day) = 2016\n" +
            "          )\n" +
            "      GROUP BY s.Sector\n" +
            "      ORDER BY AVG(ap2.Close) - AVG(ap1.Open) DESC\n" +
            "     ) sectors;";

        try {
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery(query);
            boolean f = result.next();
            gen.CreateTable("Average Performance of Sectors in 2016");

            gen.CreateRow();
            gen.InsertTableHeader("Sector");
            gen.InsertTableHeader("Price Difference");
            gen.InsertTableHeader("Performance");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.FinishRow();

                f = result.next();
            }
            gen.FinishTable();
        }  catch (Exception ee) {
            System.out.println(ee);
        }
    }

    public static void main(String args[]) {
        ReadCredentials();
        InstantiateDriver();
        ConnectToDatabase();
        assert conn != null;

        gen.CreateHTMLFile(args[0]);
        gen.InsertHeader("Stock Analytics For " + args[0], 1, "center");

        gen.InsertSeparator();
        gen.InsertSeparator();
        gen.InsertHeader("General Stock Analytics", 2, "left");

        GeneralQuery1();
        gen.InsertParagraph(
            "      The \"Securities\" table reports the total number\n" +
            "      of securities traded at the start of 2016, the\n" +
            "      total number of securities traded at the end of\n" +
            "      2016, the total number of securities whose prices\n" +
            "      saw increase between the end of 2015 and the end of\n" +
            "      2016, and the total number of securities whose\n" +
            "      prices saw decrease between the end of 2015 and the\n" +
            "      end of 2016.\n"
        );
        gen.InsertSeparator();
        GeneralQuery2();
        gen.InsertParagraph(
            "      This table reports the top 10 most heavily traded " +
            "stocks.\n"
        );
        gen.InsertSeparator();
        GeneralQuery3();
        gen.InsertParagraph(
            "      This table reports the top five highest performing\n" +
            "      stocks in terms of the absolute price increase and\n" +
            "      the top five highest performing stocks in terms of\n" +
            "      relative price increase.\n"
        );
        gen.InsertSeparator();
        gen.InsertParagraph(
            "      Based on all of the information above, these are the 10" +
            " stocks to watch in 2017 (in no particular order):<br>\n" +
            "      Priceline.com Inc<br>\n" +
            "      Chipotle Mexican Grill<br>\n" +
            "      Amazon.com Inc<br>\n" +
            "      Freeport-McMoran Cp & Gld<br>\n" +
            "      Netflix Inc.<br>\n" +
            "      Intuitive Surgical Inc.<br>\n" +
            "      Nvidia Corporation<br>\n" +
            "      Bank of America Corp<br>\n" +
            "      Chesapeake Energy<br>\n" +
            "      Micron Technology<br>\n"
        );
        gen.InsertSeparator();
        GeneralQuery4();
        gen.InsertParagraph(
            "      This table gives a general assessment of the\n" +
            "      performance of different sectors of the stock market.\n"
        );
        gen.FinishHTMLFile();
        try {
            conn.close();
        }
        catch (Exception ex) {
            System.out.println("Unable to close connection");
        }
    }
}
