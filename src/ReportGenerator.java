/*
 * ReportGenerator.java
 *
 * Members: Brian Egana (begana@calpoly.edu)
 *          Tim Jyung (tjyung@calpoly.edu)
 */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

class ReportGenerator {

    private BufferedWriter out = null;

    ReportGenerator() {}

    void CreateHTMLFile(String ticker) {
        try {
            out = new BufferedWriter(
                new FileWriter(ticker + ".html"));
            String init =
                "<!DOCTYPE html>\n" +
                "<html>\n" +
                "  <body>\n";
            out.write(init);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void AppendHTMLFile(String ticker) {
        try {
            out = new BufferedWriter(
            new FileWriter(ticker + ".html", true));
            String init =
            "<!DOCTYPE html>\n" +
            "<html>\n" +
            "  <body>\n";
            out.write(init);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void FinishHTMLFile() {
        String finish =
        "  </body>\n" +
        "</html>";
        try {
            out.write(finish);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void CreateTable(String caption) {
        String tableTag =
            "    <table border=\"1px solid black\">\n" +
            "        <caption>" + caption + "</caption>\n";
        try {
            out.write(tableTag);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void FinishTable() {
        String tableTag = "    </table>\n";
        try {
            out.write(tableTag);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void CreateRow() {
        String rowTag = "      <tr>\n";
        try {
            out.write(rowTag);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void FinishRow() {
        String rowTag = "      </tr>\n";
        try {
            out.write(rowTag);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void InsertHeader(String header, int size, String align) {
        String headerTag = "    <h" + size + " align=\"" + align + "\">" +
            header + "</h" + size + ">\n";
        try {
            out.write(headerTag);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void InsertParagraph(String paragraph) {
        String paragraphTag = "    <p>\n" + paragraph +
            "    </p>\n";
        try {
            out.write(paragraphTag);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void InsertTableHeader(String header) {
        String headerTag = "        <th>" + header + "</th>\n";
        try {
            out.write(headerTag);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void InsertTableData(String data) {
        String dataTag = "        <td>" + data + "</td>\n";
        try {
            out.write(dataTag);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void InsertSeparator() {
        String separatorTag = "    <hr>\n";
        try {
            out.write(separatorTag);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
