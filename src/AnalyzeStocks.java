/*
 * AnalyzeStocks.java
 *
 * Members: Brian Egana (begana@calpoly.edu)
 *          Tim Jyung (tjyung@calpoly.edu)
 */

public class AnalyzeStocks {
    public static void main(String args[]) {
        GeneralStockAnalyzer.main(args);
        IndividualStockAnalyzer.main(args);
    }
}
