/*
 * IndividualStockAnalyzer.java
 *
 * Members: Brian Egana (begana@calpoly.edu)
 *          Tim Jyung (tjyung@calpoly.edu)
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.*;

public class IndividualStockAnalyzer {

    private static String user = null, password = null;
    private static Connection conn = null;
    private static ReportGenerator gen = new ReportGenerator();

    private static void ReadCredentials() {
        FileReader fileReader;
        BufferedReader credentialsReader;

        try {
            fileReader = new FileReader("credentials.in");
            credentialsReader = new BufferedReader(fileReader);

            user = credentialsReader.readLine();
            password = credentialsReader.readLine();
            fileReader.close();
        }
        catch (Exception ex) {
            System.out.println("Credentials not found");
        }
    }

    private static void InstantiateDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch (Exception ex) {
            System.out.println("Driver not found");
            System.out.println(ex);
        }
    }

    private static void ConnectToDatabase() {
        String url = "jdbc:mysql://cslvm74.csc.calpoly.edu/";

        try {
            conn = DriverManager.getConnection(url + "nyse" + "?user=" + user +
            "&password=" + password);
            System.out.println("Connected");
        }
        catch (Exception ex) {
            System.out.println("Could not open connection");
        }
    }

	private static void IndividualQuery1(String ticker) {
	String query =
        "SELECT s.ticker, s.name, MIN(p.DAY) AS Day1, MAX(p.Day) AS LastDay\n" +
    	"FROM Securities s, Prices p\n" +
    	"WHERE s.ticker = ?\n" +
    	"  AND s.ticker = p.ticker;";
	
	    try {
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setString(1, ticker);
            ResultSet result = statement.executeQuery();
            boolean f = result.next();
            gen.CreateTable("Range of Dates for which Pricing is Available");

            gen.CreateRow();
            gen.InsertTableHeader("Stock");
            gen.InsertTableHeader("First Day");
            gen.InsertTableHeader("Last Day");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(3);
                String s3 = result.getString(4);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.FinishRow();
                f = result.next();
            }
            gen.FinishTable();
        }  catch (Exception ee) {
            System.out.println(ee);
        }
	}
	
	private static void IndividualQuery2(String ticker) {
	    String query =
            "SELECT data.Year, data.ticker, ap2.Close - ap1.Open AS PriceChange,\n" +
            "    data.Volume, data.AvgClosingPrice, data.AvgVolume\n" +
        	"FROM Securities s, AdjustedPrices ap1, AdjustedPrices ap2,\n" +
        	"    (\n" +
            "     SELECT s.ticker, YEAR(ap.day) AS Year, MIN(ap.DAY) AS Day1,\n" +
            "         MAX(ap.Day) AS LastDay, SUM(ap.Volume) AS Volume,\n" +
            "         AVG(ap.Close) AS AvgClosingPrice, AVG(ap.Volume) AS AvgVolume\n" +
        	"     FROM Securities s, AdjustedPrices ap\n" +
        	"     WHERE s.ticker = ? AND s.ticker = ap.ticker\n" +
        	"     GROUP BY Year\n" +
            "    ) data\n" +
        	"WHERE s.ticker = ? AND s.ticker = ap1.ticker AND\n" +
            "    s.ticker = ap2.ticker AND ap1.day = data.Day1 AND \n" +
            "    ap2.day = data.LastDay;";
	
        try {
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setString(1, ticker);
            statement.setString(2, ticker);
            ResultSet result = statement.executeQuery();
            boolean f = result.next();
            gen.CreateTable("Stock Performance For Every Year");

            gen.CreateRow();
            gen.InsertTableHeader("Year");
            gen.InsertTableHeader("Ticker");
            gen.InsertTableHeader("Increase/Decrease in Prices Year over Year");
            gen.InsertTableHeader("Average Closing Price");
            gen.InsertTableHeader("Average Trade Volume Per Day");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);
                String s4 = result.getString(4);
                String s5 = result.getString(5);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.InsertTableData(s4);
                gen.InsertTableData(s5);
                gen.FinishRow();
                f = result.next();
            }
            gen.FinishTable();
        }  catch (Exception ee) {
            System.out.println(ee);
        }
	}
	private static void IndividualQuery3(String ticker) {
    	String query =
            "SELECT MONTH(ap.day) AS Month, ap.ticker,\n" +
            "    AVG(ap.close) AS AvgClosingPrice, MAX(ap.high) AS High,\n" +
            "    MIN(ap.low) AS Low, AVG(ap.volume) AS AvgVolume\n" +
        	"FROM AdjustedPrices ap\n" +
        	"WHERE ap.ticker = ? AND YEAR(ap.day) = 2016\n" +
        	"GROUP BY Month\n" +
        	"ORDER BY Month(ap.Day);";

    	try {
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setString(1, ticker);
            ResultSet result = statement.executeQuery();
            boolean f = result.next();
            gen.CreateTable("Stock Data For 2016");

            gen.CreateRow();
            gen.InsertTableHeader("Month");
            gen.InsertTableHeader("Ticker");
            gen.InsertTableHeader("Average Closing Price");
            gen.InsertTableHeader("Highest Price");
            gen.InsertTableHeader("Lowest Price");
            gen.InsertTableHeader("Average Trade Volume Per Month");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);
                String s4 = result.getString(4);
                String s5 = result.getString(5);
                String s6 = result.getString(6);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.InsertTableData(s4);
                gen.InsertTableData(s5);
                gen.InsertTableData(s6);
                gen.FinishRow();
                f = result.next();
            }
            gen.FinishTable();
        }  catch (Exception ee) {
            System.out.println(ee);
        }
	}
	
	private static void IndividualQuery4(String ticker) {
	    String query =
            "SELECT counts.*\n" +
	        "FROM (\n" +
            "      SELECT data.year, data.month, data.AvgClosingPrice\n" +
    	    "      FROM (\n" +
            "            SELECT Year(ap.day) AS year, Month(ap.day) AS month,\n" +
            "                AVG(ap.close) AS AvgClosingPrice\n" +
        	"            FROM AdjustedPrices ap, Securities s\n"  +
        	"            WHERE s.ticker = ap.ticker AND s.ticker = ?\n" +
        	"            GROUP BY s.Sector,year, month" +
            "           ) data\n" +
        	"      GROUP BY data.year, data.month\n" +
            "     ) counts,\n" +
    	    "     (\n" +
            "      SELECT counts.year, MAX(counts.AvgClosingPrice) AS max\n" +
        	"      FROM (\n" +
            "            SELECT data.year, data.month, data.AvgClosingPrice\n" +
        	"            FROM (\n" +
            "                  SELECT Year(ap.day) AS year, Month(ap.day) AS month,\n " +
            "                      AVG(ap.close) AS AvgClosingPrice\n" +
    	    "                  FROM AdjustedPrices ap, Securities s\n" +
        	"                  WHERE s.ticker = ap.ticker AND s.ticker = ?\n" +
    	    "                  GROUP BY s.Sector,year, month\n" +
            "                 ) data\n" +
        	"            GROUP BY data.year, data.month\n" +
            "           ) counts\n" +
        	"      GROUP BY counts.year\n" +
            "     ) max\n" +
    	    "WHERE max.max = counts.AvgClosingPrice\n" +
    	    "GROUP BY counts.year;";

    	try {
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setString(1, ticker);
            statement.setString(2, ticker);
            ResultSet result = statement.executeQuery();
            boolean f = result.next();
            gen.CreateTable("Best Month Performance");

            gen.CreateRow();
            gen.InsertTableHeader("Year");
            gen.InsertTableHeader("Month");
            gen.InsertTableHeader("Average Closing Price");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.FinishRow();
                f = result.next();
            }
            gen.FinishTable();
        }  catch (Exception ee) {
            System.out.println(ee);
        }
	}
	
    private static void SetStatementParameters(PreparedStatement statement,
        String ticker, String date, String threeMonthsBefore, int month, int year) {
        try {
            statement.setString(1, ticker);
            statement.setDate(2, Date.valueOf(date));
            statement.setDate(3, Date.valueOf(threeMonthsBefore));
            statement.setString(4, ticker);
            statement.setString(5, ticker);
            statement.setInt(6, month);
            statement.setInt(7, year);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
	
    private static void IndividualQuery5(String ticker) {
        String query =
            "SELECT ap.Volume AS DayVol, Avg.Volume AS AvgVol, ap.Close AS DayClose,\n" +
            "    Avg.Close AS AvgClose,\n" +
            "    IF(ap.Volume < Avg.Volume,\n" +
            "       IF(ap.Close > Avg.Close, 'Sell', 'Hold'),\n" +
            "       IF(ap.Close > Avg.Close, 'Buy', 'Hold')\n" +
            "      ) AS TradeDecision\n" +
            "FROM (\n" +
            "      SELECT AVG(Last3MonFromDate.Close) AS Close,\n" +
            "          AVG(Last3MonFromDate.Volume) AS Volume\n" +
            "      FROM (\n" +
            "            SELECT s.Ticker, ap.Day, ap.Close, ap.Volume\n" +
            "            FROM Securities s NATURAL JOIN AdjustedPrices ap\n" +
            "            WHERE s.Ticker = ? AND ap.Day < ? AND\n" +
            "                ap.Day >= ?\n" +
            "           ) Last3MonFromDate\n" +
            "     ) Avg, Securities s NATURAL JOIN AdjustedPrices ap\n" +
            "WHERE s.Ticker = ? AND ap.Day =\n" +
            "    (\n" +
            "     SELECT MIN(Day)\n" +
            "     FROM Securities s NATURAL JOIN AdjustedPrices ap\n" +
            "     WHERE s.Ticker = ? AND MONTH(Day) = ? AND YEAR(Day) = ?\n" +
            "    );\n";

        try {
            PreparedStatement statement = conn.prepareStatement(query);
            SetStatementParameters(statement, ticker, "2015-01-01",
            "2014-10-01", 1, 2015);
            ResultSet result = statement.executeQuery();
            boolean f = result.next();
            gen.CreateTable("Trade Decision Before January 2015");

            gen.CreateRow();
            gen.InsertTableHeader("Volume of Day Before 1st");
            gen.InsertTableHeader("Average Volume of Last Three Months");
            gen.InsertTableHeader("Closing Price of Day Before 1st");
            gen.InsertTableHeader("Average Closing Price of Last Three Months");
            gen.InsertTableHeader("Decision");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);
                String s4 = result.getString(4);
                String s5 = result.getString(5);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.InsertTableData(s4);
                gen.InsertTableData(s5);
                gen.FinishRow();

                f = result.next();
            }
            gen.FinishTable();

            statement.clearParameters();
            SetStatementParameters(statement, ticker, "2015-06-01",
            "2015-03-01", 6, 2015);
            result = statement.executeQuery();
            f = result.next();
            gen.CreateTable("Trade Decision Before June 2015");

            gen.CreateRow();
            gen.InsertTableHeader("Volume of Day Before 1st");
            gen.InsertTableHeader("Average Volume of Last Three Months");
            gen.InsertTableHeader("Closing Price of Day Before 1st");
            gen.InsertTableHeader("Average Closing Price of Last Three Months");
            gen.InsertTableHeader("Decision");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);
                String s4 = result.getString(4);
                String s5 = result.getString(5);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.InsertTableData(s4);
                gen.InsertTableData(s5);
                gen.FinishRow();

                f = result.next();
            }
            gen.FinishTable();

            statement.clearParameters();
            SetStatementParameters(statement, ticker, "2015-10-01",
            "2015-07-01", 10, 2015);
            result = statement.executeQuery();
            f = result.next();
            gen.CreateTable("Trade Decision Before October 2015");

            gen.CreateRow();
            gen.InsertTableHeader("Volume of Day Before 1st");
            gen.InsertTableHeader("Average Volume of Last Three Months");
            gen.InsertTableHeader("Closing Price of Day Before 1st");
            gen.InsertTableHeader("Average Closing Price of Last Three Months");
            gen.InsertTableHeader("Decision");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);
                String s4 = result.getString(4);
                String s5 = result.getString(5);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.InsertTableData(s4);
                gen.InsertTableData(s5);
                gen.FinishRow();

                f = result.next();
            }
            gen.FinishTable();

            statement.clearParameters();
            SetStatementParameters(statement, ticker, "2016-01-01",
            "2015-10-01", 1, 2016);
            result = statement.executeQuery();
            f = result.next();
            gen.CreateTable("Trade Decision Before January 2016");

            gen.CreateRow();
            gen.InsertTableHeader("Volume of Day Before 1st");
            gen.InsertTableHeader("Average Volume of Last Three Months");
            gen.InsertTableHeader("Closing Price of Day Before 1st");
            gen.InsertTableHeader("Average Closing Price of Last Three Months");
            gen.InsertTableHeader("Decision");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);
                String s4 = result.getString(4);
                String s5 = result.getString(5);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.InsertTableData(s4);
                gen.InsertTableData(s5);
                gen.FinishRow();

                f = result.next();
            }
            gen.FinishTable();

            statement.clearParameters();
            SetStatementParameters(statement, ticker, "2016-06-01",
            "2016-03-01", 6, 2016);
            result = statement.executeQuery();
            f = result.next();
            gen.CreateTable("Trade Decision Before June 2016");

            gen.CreateRow();
            gen.InsertTableHeader("Volume of Day Before 1st");
            gen.InsertTableHeader("Average Volume of Last Three Months");
            gen.InsertTableHeader("Closing Price of Day Before 1st");
            gen.InsertTableHeader("Average Closing Price of Last Three Months");
            gen.InsertTableHeader("Decision");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);
                String s4 = result.getString(4);
                String s5 = result.getString(5);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.InsertTableData(s4);
                gen.InsertTableData(s5);
                gen.FinishRow();

                f = result.next();
            }
            gen.FinishTable();

            statement.clearParameters();
            SetStatementParameters(statement, ticker, "2016-10-01",
            "2016-07-01", 10, 2016);
            result = statement.executeQuery();
            f = result.next();
            gen.CreateTable("Trade Decision Before October 2016");

            gen.CreateRow();
            gen.InsertTableHeader("Volume of Day Before 1st");
            gen.InsertTableHeader("Average Volume of Last Three Months");
            gen.InsertTableHeader("Closing Price of Day Before 1st");
            gen.InsertTableHeader("Average Closing Price of Last Three Months");
            gen.InsertTableHeader("Decision");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);
                String s4 = result.getString(4);
                String s5 = result.getString(5);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.InsertTableData(s4);
                gen.InsertTableData(s5);
                gen.FinishRow();

                f = result.next();
            }
            gen.FinishTable();
        }  catch (Exception ee) {
            System.out.println(ee);
        }
    }
    
    private static void IndividualQuery6(String ticker) {
        String query =
        "SELECT ap.Volume AS DayVol, Avg.Volume AS AvgVol, ap.Close AS DayClose,\n" +
        "    Avg.Close AS AvgClose,\n" +
        "    IF(ap.Volume < Avg.Volume,\n" +
        "       IF(ap.Close > Avg.Close, 'Sell', 'Hold'),\n" +
        "       IF(ap.Close > Avg.Close, 'Buy', 'Hold')\n" +
        "      ) AS TradeDecision\n" +
        "FROM (\n" +
        "      SELECT AVG(Last3MonFromDate.Close) AS Close,\n" +
        "          AVG(Last3MonFromDate.Volume) AS Volume\n" +
        "      FROM (\n" +
        "            SELECT s.Ticker, ap.Day, ap.Close, ap.Volume\n" +
        "            FROM Securities s NATURAL JOIN AdjustedPrices ap\n" +
        "            WHERE s.Ticker = ? AND ap.Day < ? AND\n" +
        "                ap.Day >= ?\n" +
        "           ) Last3MonFromDate\n" +
        "     ) Avg, Securities s NATURAL JOIN AdjustedPrices ap\n" +
        "WHERE s.Ticker = ? AND ap.Day =\n" +
        "    (\n" +
        "     SELECT MIN(Day)\n" +
        "     FROM Securities s NATURAL JOIN AdjustedPrices ap\n" +
        "     WHERE s.Ticker = ? AND MONTH(Day) = ? AND YEAR(Day) = ?\n" +
        "    );\n";

        try {
            PreparedStatement statement = conn.prepareStatement(query);
            SetStatementParameters(statement, ticker, "2015-03-01",
            "2015-01-01", 1, 2015);
            ResultSet result = statement.executeQuery();
            boolean f = result.next();
            gen.CreateTable("Trade Decision 3 Months After January 2015");

            gen.CreateRow();
            gen.InsertTableHeader("Volume of Day Before 1st");
            gen.InsertTableHeader("Average Volume of Last Three Months");
            gen.InsertTableHeader("Closing Price of Day Before 1st");
            gen.InsertTableHeader("Average Closing Price of Last Three Months");
            gen.InsertTableHeader("Decision");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);
                String s4 = result.getString(4);
                String s5 = result.getString(5);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.InsertTableData(s4);
                gen.InsertTableData(s5);
                gen.FinishRow();

                f = result.next();
            }
            gen.FinishTable();

            statement.clearParameters();
            SetStatementParameters(statement, ticker, "2015-09-01",
            "2015-06-01", 6, 2015);
            result = statement.executeQuery();
            f = result.next();
            gen.CreateTable("Trade Decision 3 Months After June 2015");

            gen.CreateRow();
            gen.InsertTableHeader("Volume of Day Before 1st");
            gen.InsertTableHeader("Average Volume of Last Three Months");
            gen.InsertTableHeader("Closing Price of Day Before 1st");
            gen.InsertTableHeader("Average Closing Price of Last Three Months");
            gen.InsertTableHeader("Decision");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);
                String s4 = result.getString(4);
                String s5 = result.getString(5);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.InsertTableData(s4);
                gen.InsertTableData(s5);
                gen.FinishRow();

                f = result.next();
            }
            gen.FinishTable();

            statement.clearParameters();
            SetStatementParameters(statement, ticker, "2016-01-01",
            "2015-10-01", 10, 2015);
            result = statement.executeQuery();
            f = result.next();
            gen.CreateTable("Trade Decision 3 Months After October 2015");

            gen.CreateRow();
            gen.InsertTableHeader("Volume of Day Before 1st");
            gen.InsertTableHeader("Average Volume of Last Three Months");
            gen.InsertTableHeader("Closing Price of Day Before 1st");
            gen.InsertTableHeader("Average Closing Price of Last Three Months");
            gen.InsertTableHeader("Decision");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);
                String s4 = result.getString(4);
                String s5 = result.getString(5);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.InsertTableData(s4);
                gen.InsertTableData(s5);
                gen.FinishRow();

                f = result.next();
            }
            gen.FinishTable();

            statement.clearParameters();
            SetStatementParameters(statement, ticker, "2016-03-01",
            "2016-01-01", 1, 2016);
            result = statement.executeQuery();
            f = result.next();
            gen.CreateTable("Trade Decision 3 Months After January 2016");

            gen.CreateRow();
            gen.InsertTableHeader("Volume of Day Before 1st");
            gen.InsertTableHeader("Average Volume of Last Three Months");
            gen.InsertTableHeader("Closing Price of Day Before 1st");
            gen.InsertTableHeader("Average Closing Price of Last Three Months");
            gen.InsertTableHeader("Decision");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);
                String s4 = result.getString(4);
                String s5 = result.getString(5);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.InsertTableData(s4);
                gen.InsertTableData(s5);
                gen.FinishRow();

                f = result.next();
            }
            gen.FinishTable();

            statement.clearParameters();
            SetStatementParameters(statement, ticker, "2016-09-01",
            "2016-06-01", 6, 2016);
            result = statement.executeQuery();
            f = result.next();
            gen.CreateTable("Trade Decision 3 Months After June 2016");

            gen.CreateRow();
            gen.InsertTableHeader("Volume of Day Before 1st");
            gen.InsertTableHeader("Average Volume of Last Three Months");
            gen.InsertTableHeader("Closing Price of Day Before 1st");
            gen.InsertTableHeader("Average Closing Price of Last Three Months");
            gen.InsertTableHeader("Decision");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);
                String s4 = result.getString(4);
                String s5 = result.getString(5);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.InsertTableData(s4);
                gen.InsertTableData(s5);
                gen.FinishRow();

                f = result.next();
            }
            gen.FinishTable();

            statement.clearParameters();
            SetStatementParameters(statement, ticker, "2017-01-01",
            "2016-10-01", 10, 2016);
            result = statement.executeQuery();
            f = result.next();
            gen.CreateTable("Trade Decision 3 Months After October 2016");

            gen.CreateRow();
            gen.InsertTableHeader("Volume of Day Before 1st");
            gen.InsertTableHeader("Average Volume of Last Three Months");
            gen.InsertTableHeader("Closing Price of Day Before 1st");
            gen.InsertTableHeader("Average Closing Price of Last Three Months");
            gen.InsertTableHeader("Decision");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);
                String s4 = result.getString(4);
                String s5 = result.getString(5);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.InsertTableData(s4);
                gen.InsertTableData(s5);
                gen.FinishRow();

                f = result.next();
            }
            gen.FinishTable();
        }  catch (Exception ee) {
            System.out.println(ee);
        }
    }
    
    private static void IndividualQuery7(String ticker) {
	    String query =
            "SELECT Month, Ticker, Top, PriceDif, VolumeDif\n" +
        	"FROM (\n" +
            "      SELECT MONTH(p1.day) AS Month, p1.ticker AS Ticker, p2.ticker AS Top,\n" +
        	"          p1.close - p2.close AS PriceDif,\n" +
            "          p1TotalVol.vol - p2TotalVol.vol AS VolumeDif\n" +
        	"      FROM AdjustedPrices p1, AdjustedPrices p2,\n" +
        	"           (\n" +
            "            SELECT MIN(day) AS StartDate\n" +
        	"            FROM AdjustedPrices p\n" +
        	"            WHERE YEAR(p.day) = 2016\n" +
        	"            GROUP BY MONTH(p.day)\n" +
            "           ) date,\n" +
        	"           (\n" +
            "            SELECT MONTH(p.day) AS mon, p.ticker, SUM(volume) AS vol\n"+
        	"            FROM AdjustedPrices p\n"+
        	"            WHERE YEAR(p.day) = 2016\n"+
        	"            GROUP BY MONTH(p.day), p.ticker\n" +
            "           ) p1TotalVol,\n"+
        	"           (\n" +
            "            SELECT MONTH(p.day) AS mon, p.ticker, SUM(volume) AS vol\n"+
        	"            FROM AdjustedPrices p\n"+
        	"            WHERE YEAR(p.day) = 2016\n"+
        	"            GROUP BY MONTH(p.day), p.ticker\n" +
            "           ) p2TotalVol,\n"+
        	"           (\n" +
            "            SELECT s.Ticker AS Ticker\n"+
        	"            FROM Securities s NATURAL JOIN AdjustedPrices ap\n"+
        	"            WHERE YEAR(ap.Day) = 2016\n"+
        	"            GROUP BY s.Name\n"+
        	"            ORDER BY SUM(ap.Volume) DESC LIMIT 5\n" +
            "           ) topFive\n"+
        	"      WHERE p1.ticker = ? AND p2.ticker = topFive.Ticker\n" +
        	"          AND YEAR(p1.day) = 2016 AND YEAR(p2.day) = 2016\n" +
        	"          AND p1.day = date.startDate AND p2.day = date.startDate\n" +
        	"          AND MONTH(p1.day) = p1TotalVol.mon AND MONTH(p2.day) = p2TotalVol.mon\n" +
        	"          AND p1TotalVol.ticker = p1.ticker AND p2TotalVol.ticker = p2.ticker\n" +
        	"      ORDER BY p1.day, p2.ticker\n" +
            "     ) data;";
	
    	try {
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setString(1, ticker);
            ResultSet result = statement.executeQuery();
            boolean f = result.next();
            gen.CreateTable("Top Performing Stocks Comparison");

            gen.CreateRow();
            gen.InsertTableHeader("Month");
            gen.InsertTableHeader("Ticker");
            gen.InsertTableHeader("Top Five Stocks");
            gen.InsertTableHeader("Price Difference");
            gen.InsertTableHeader("Volume Difference");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);
                String s4 = result.getString(4);
                String s5 = result.getString(5);

                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.InsertTableData(s4);
                gen.InsertTableData(s5);
                gen.FinishRow();
                f = result.next();
            }
            gen.FinishTable();
        }  catch (Exception ee) {
            System.out.println(ee);
        }
	}
	
	private static void IndividualQuery8(String ticker, String ticker2) {
	    String query =
            "SELECT Month, Ticker1, Ticker2, PriceDif, VolumeDif\n" +
        	"FROM (\n" +
            "      SELECT MONTH(p1.day) AS Month, p1.ticker AS Ticker1, p2.ticker AS Ticker2,\n"+
            "          p1.close - p2.close AS PriceDif,\n" +
            "          p1TotalVol.vol - p2TotalVol.vol AS VolumeDif\n" +
        	"      FROM AdjustedPrices p1, AdjustedPrices p2,\n" +
        	"           (\n" +
            "            SELECT MIN(day) AS StartDate\n" +
        	"            FROM AdjustedPrices p\n" +
            "            WHERE YEAR(p.day) = 2016\n" +
        	"            GROUP BY MONTH(p.day)\n" +
            "           ) date,\n" +
        	"           (\n" +
            "            SELECT MONTH(p.day) AS mon, p.ticker, SUM(volume) AS vol\n" +
        	"            FROM AdjustedPrices p\n" +
        	"            WHERE YEAR(p.day) = 2016\n" +
        	"            GROUP BY MONTH(p.day), p.ticker\n" +
            "           ) p1TotalVol,\n" +
            "           (\n" +
            "            SELECT MONTH(p.day) AS mon, p.ticker, SUM(volume) AS vol\n" +
        	"            FROM AdjustedPrices p\n" +
        	"            WHERE YEAR(p.day) = 2016\n" +
        	"            GROUP BY MONTH(p.day), p.ticker\n" +
            "           ) p2TotalVol\n" +
        	"      WHERE p1.ticker = ? AND p2.ticker = ? AND YEAR(p1.day) = 2016\n" +
        	"          AND YEAR(p2.day) = 2016 AND p1.day = date.startDate\n" +
        	"          AND p2.day = date.startDate AND MONTH(p1.day) = p1TotalVol.mon\n" +
        	"          AND MONTH(p2.day) = p2TotalVol.mon AND p1TotalVol.ticker = p1.ticker\n" +
        	"          AND p2TotalVol.ticker = p2.ticker\n" +
        	"      ORDER BY p1.day, p2.ticker\n" +
            "     ) data;";
	
        try {
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setString(1, ticker);
            statement.setString(2, ticker2);
            ResultSet result = statement.executeQuery();
            boolean f = result.next();
            gen.CreateTable("Two Stocks Comparison");

            gen.CreateRow();
            gen.InsertTableHeader("Month");
            gen.InsertTableHeader("Ticker 1");
            gen.InsertTableHeader("Ticker 2");
            gen.InsertTableHeader("Price Difference");
            gen.InsertTableHeader("Volume Difference");
            gen.FinishRow();

            while (f) {
                String s1 = result.getString(1);
                String s2 = result.getString(2);
                String s3 = result.getString(3);
                String s4 = result.getString(4);
                String s5 = result.getString(5);
                gen.CreateRow();
                gen.InsertTableData(s1);
                gen.InsertTableData(s2);
                gen.InsertTableData(s3);
                gen.InsertTableData(s4);
                gen.InsertTableData(s5);
                gen.FinishRow();
                f = result.next();
            }
            gen.FinishTable();
        }  catch (Exception ee) {
            System.out.println(ee);
        }
	}
	
	public static void main(String args[]) {
        ReadCredentials();
        InstantiateDriver();
        ConnectToDatabase();
        assert conn != null;
        
        String ticker = args[0];
        String ticker2 = args[1];

        gen.AppendHTMLFile(args[0]);
        gen.InsertSeparator();
        gen.InsertSeparator();
        gen.InsertHeader("Individual Stock Analytics", 2, "left");

        IndividualQuery1(ticker);
        gen.InsertParagraph(
			"      Range of dates for which the pricing data is available."
        );
        gen.InsertSeparator();
        IndividualQuery2(ticker);
        gen.InsertParagraph(
			"      In this table we calculated the increase/decrease in prices year-over-year,"+
			" volume of trading, average closing price in a year, and average trade volume per day."
		); 
        gen.InsertSeparator();
        IndividualQuery3(ticker);
		gen.InsertParagraph(
			"      For 2016, we showed the average closing price, high and low price and"+
			" the average daily trading volume by month."
		); 
        gen.InsertSeparator();
        IndividualQuery4(ticker);
        gen.InsertParagraph(
			"      This table we calculate the best month of performance for each year calculated"+
			" based off their average closing price."
		); 
        gen.InsertSeparator();
        IndividualQuery5(ticker);
        gen.InsertParagraph(
            "      This table shows buy, hold and sell predictions using past data." +
            "We based our predictions off of volume and price.\n"
        );
        gen.InsertSeparator();
        IndividualQuery6(ticker);
        gen.InsertParagraph(
            "      This table shows the buy, hold and sell calculations of the past 3 months." +
            "We based our predictions off of volume and price.\n"
        );
        gen.InsertSeparator();
        IndividualQuery7(ticker);
		gen.InsertParagraph(
			"      This table compares the " + args[0] + " stock with the top five stocks " +
        "according to their prices and volumes.\n"
		);
        gen.InsertSeparator();
        IndividualQuery8(ticker, ticker2);
        gen.InsertParagraph(
        "      This table compares the " + args[0] + " stock with the " + args[1] +
        " stock according to their prices and volumes.\n"
        );
        gen.FinishHTMLFile();
        try {
            conn.close();
        }
        catch (Exception ex) {
            System.out.println("Unable to close connection");
        }
    }
}
